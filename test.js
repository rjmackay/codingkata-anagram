import { assert } from 'chai';
import getAnagrams, { hashWord, getGroupedAnagrams } from './getAnagrams';

describe('getAnagrams()', () => {
  it('should return anagrams of cat', () => {
    assert.sameMembers(
      ['cat', 'tac', 'act'],
      getAnagrams(['cat', 'dog', 'tac', 'god', 'act'], 'cat'),
    );
  });
});

describe('hasWord()', () => {
  it('should return lowercase letters in alpha order', () => {
    assert.equal('cdeiinnt', hashWord('incident'));
  });
});

describe('getGroupedAnagrams()', () => {
  it('should return grouped anagrams', () => {
    assert.deepEqual({
      act: ['cat', 'tac', 'act'],
      dgo: ['dog', 'god'],
    }, getGroupedAnagrams(['cat', 'dog', 'tac', 'god', 'act']));
  });
});
