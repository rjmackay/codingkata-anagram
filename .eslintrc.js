module.exports = {
  "extends": "airbnb-base",
  "rules": {
    "no-console": 0,
  },
  "plugins": [
    "mocha"
  ],
  "globals": {
    "describe": true,
    "it": true
  }
};