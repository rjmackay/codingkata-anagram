# codingkata-anagrams

Given an input word, and a dictionary file such as https://github.com/paritytech/wordlist/blob/master/res/wordlist.txt, print all the words within the dictionary that are anagrams (same letters, different arrangement) of the input word.

## To install and run

- `npm install`
- `npm run start -- turbine`

Or to specify a word list

- `npm run start -- -l "wordlist.txt" turbine`

## Tests

Run `npm run test`

## Lint

Run `npm run lint`