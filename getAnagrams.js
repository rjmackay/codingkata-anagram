export const hashWord = word => word.toLowerCase().split('').sort().join('');

export const getGroupedAnagrams = words => words.reduce((anagrams, word) => {
  const hash = hashWord(word);
  anagrams[hash] = anagrams[hash] || [];
  anagrams[hash].push(word);
  return anagrams;
}, {});

export default (words, wordInput) => {
  const anagrams = getGroupedAnagrams(words);

  return anagrams[hashWord(wordInput)];
};
