#!/usr/bin/env node

import program from 'commander';
import fs from 'fs';
import getAnagrams from './getAnagrams';

let wordInput;
let wordlist;

program
  .version('1.0.0')
  .arguments('<word>')
  .option('-l, --wordlist [filename]', 'Word list file')
  .action((word) => {
    wordInput = word;
  })
  .parse(process.argv);

if (typeof wordInput === 'undefined') {
  console.error('No word given!');
  process.exit(1);
}

if (program.wordlist) {
  wordlist = program.wordlist;
} else {
  wordlist = 'wordlist.txt';
}

console.log('Using word list:', wordlist);
console.log('Getting anagrams for:', wordInput);

const data = fs.readFileSync(wordlist).toString().split('\n');

const result = getAnagrams(data, wordInput);
if (result) {
  console.log(result.join('\n'));
} else {
  console.log('No anagrams found');
}
